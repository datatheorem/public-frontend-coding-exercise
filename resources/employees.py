import json
from typing import Dict, List

import falcon
from pydantic import ValidationError

from pydantic_models import Employee, EmployeeCreate

DEFAULT_PAGE_SIZE: int = 100
FILTER_FIELDS: List[str] = ["department", "job_titles"]


class BaseResource:
    def __init__(self, db: Dict[int, Employee]):
        self.db = db


class EmployeeResource(BaseResource):
    def on_get_collection(self, req: falcon.Request, resp: falcon.Response) -> None:
        page_size = req.get_param_as_int("per_page", default=DEFAULT_PAGE_SIZE)
        page_index = req.get_param_as_int("page", default=1)

        employees_list: List[Employee] = list(self.db.values())
        for filter_field in FILTER_FIELDS:
            if req.get_param(filter_field, default=None):
                employees_list = [
                    employee
                    for employee in employees_list
                    if getattr(employee, filter_field) == req.get_param(filter_field)
                ]
        offset = (page_index - 1) * page_size
        result = [employee.dict() for employee in employees_list][offset: offset + page_size]
        resp.body = json.dumps(result)
        resp.status = falcon.HTTP_200

    def on_post_collection(self, req: falcon.Request, resp: falcon.Response) -> None:
        try:
            new_employee_input = EmployeeCreate.parse_obj(req.media)
        except ValidationError as e:
            resp.status = falcon.HTTP_400
            resp.body = e.json()
            return
        new_sid: int = max(self.db.keys()) + 1
        new_employee = Employee(id=new_sid, **new_employee_input.dict())
        self.db[new_sid] = new_employee
        resp.status = falcon.HTTP_201
        resp.body = json.dumps(new_employee.dict())

    def on_get(self, req: falcon.Request, resp: falcon.Response, employee_id: int) -> None:
        employee = self.db.get(employee_id, None)
        if employee:
            resp.status = falcon.HTTP_200
            resp.body = json.dumps(employee.dict())
        else:
            resp.status = falcon.HTTP_404
            resp.body = json.dumps({"error": "Not Found"})
