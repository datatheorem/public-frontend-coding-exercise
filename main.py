import json
from pathlib import Path
from typing import Dict

import falcon

from pydantic_models import Employee
from resources.employees import EmployeeResource


class HandleCORS:
    def process_request(self, req: falcon.Request, resp: falcon.Response) -> None:
        resp.set_header("Access-Control-Allow-Origin", "*")
        resp.set_header("Access-Control-Allow-Headers", "Content-Type")
        resp.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")


def load_data(file_name: str) -> Dict[int, Employee]:
    with Path(file_name).open("r") as file:
        raw_data = json.load(file)

    fields_name = [column["fieldName"] for column in raw_data["meta"]["view"]["columns"]]
    employees = {}
    for data in raw_data["data"]:
        employee = {}
        for field_name, value in zip(fields_name, data):
            # Convert the field `:sid` to `id` (for simplicity)
            if field_name == ":sid":
                field_name = "id"

            # Skip the special fields
            if not field_name.startswith(":"):
                employee[field_name] = value

        # Add the employee in the DB
        employees[employee["id"]] = Employee.parse_obj(employee)

    return employees


def create_falcon_api(json_file: str) -> falcon.API:
    api = falcon.API(middleware=[HandleCORS()])
    db = load_data(json_file)
    employee = EmployeeResource(db)
    api.add_route("/", employee, suffix="collection")  # list and post employees
    api.add_route("/{employee_id:int}", employee)  # get single employee
    return api


app = create_falcon_api("employee.json")
