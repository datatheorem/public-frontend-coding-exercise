from typing import Any

from pydantic import BaseModel, validator


class EmployeeBase(BaseModel):
    name: str
    job_titles: str
    department: str
    employee_annual_salary: str

    @validator("employee_annual_salary", pre=True)
    def validate_employee_annual_salary(cls, v: Any) -> str:
        if isinstance(v, str) and v.replace(".", "", 1).isdigit():
            return "{0:.2f}".format(float(v))
        if isinstance(v, int) or (isinstance(v, float) and v >= 0):
            return "{0:.2f}".format(v)
        raise TypeError("`employee_annual_salary` must be an int or a positive float")


class EmployeeCreate(EmployeeBase):
    pass


class Employee(EmployeeBase):
    id: int
