FROM python:3.7-slim

WORKDIR /code
COPY . .
RUN pip install pipenv
RUN pipenv install

EXPOSE 8000
CMD [ "pipenv", "run", "gunicorn", "-b", "0.0.0.0:8000", "main:app" ]