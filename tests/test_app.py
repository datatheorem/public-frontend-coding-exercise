import json
from typing import List

import falcon
import pytest
from falcon import testing

from main import app
from pydantic_models import EmployeeCreate
from resources.employees import DEFAULT_PAGE_SIZE


@pytest.fixture
def client():
    return testing.TestClient(app)


def test_get_employee(client):
    response = client.simulate_get("/1")
    assert response.status == falcon.HTTP_200
    assert response.json == {
        "id": 1,
        "name": "AARON,  ELVIA J",
        "job_titles": "WATER RATE TAKER",
        "department": "WATER MGMNT",
        "employee_annual_salary": "90744.00",
    }


def test_get_missing_employee(client):
    response = client.simulate_get("/99999")
    assert response.status == falcon.HTTP_404
    assert response.json == {"error": "Not Found"}


def test_list_employees(client):
    response = client.simulate_get("/")
    assert response.status == falcon.HTTP_200
    assert len(response.json) == DEFAULT_PAGE_SIZE
    assert response.json[0] == {
        "id": 1,
        "name": "AARON,  ELVIA J",
        "job_titles": "WATER RATE TAKER",
        "department": "WATER MGMNT",
        "employee_annual_salary": "90744.00",
    }


def test_list_employees_pagination(client):
    page_size = 10
    page_index = 2
    response = client.simulate_get("/", params={"per_page": page_size, "page": page_index})
    assert response.status == falcon.HTTP_200
    assert len(response.json) == page_size
    assert response.json[0]["id"] == (page_index - 1) * page_size + 1


def test_list_employees_filters(client):
    response = client.simulate_get("/", params={"department": "POLICE"})
    assert response.status == falcon.HTTP_200
    assert all((employee["department"] == "POLICE" for employee in response.json))

    response = client.simulate_get("/", params={"job_titles": "POLICE OFFICER"})
    assert response.status == falcon.HTTP_200
    assert all((employee["job_titles"] == "POLICE OFFICER" for employee in response.json))


def test_post_employee(client):
    payload = {
        "department": "COMPUTER",
        "employee_annual_salary": "10000000000.0",
        "job_titles": "CODER",
        "name": "John Doe",
    }
    response = client.simulate_post(body=json.dumps(payload))
    assert response.status == falcon.HTTP_201
    assert response.json == {
        "id": 32063,
        "name": "John Doe",
        "job_titles": "CODER",
        "department": "COMPUTER",
        "employee_annual_salary": "10000000000.00",
    }


def test_post_employee_required_fields(client):
    payload = {}
    response = client.simulate_post(body=json.dumps(payload))
    assert response.status == falcon.HTTP_400
    required_fields: List[str] = [
        field for (field, definition) in EmployeeCreate.__fields__.items() if definition.required
    ]
    assert response.json == [
        {"loc": [missing_field], "msg": "field required", "type": "value_error.missing"}
        for missing_field in required_fields
    ]


def test_post_employee_salary_validation(client):
    payload = {"department": "COMPUTER", "employee_annual_salary": "", "job_titles": "CODER", "name": "John Doe"}
    response = client.simulate_post(body=json.dumps(payload))
    assert response.status == falcon.HTTP_400
    assert response.json == [
        {
            "loc": ["employee_annual_salary"],
            "msg": "`employee_annual_salary` must be an int or a positive float",
            "type": "type_error",
        }
    ]
